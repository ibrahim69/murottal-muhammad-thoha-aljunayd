function getParam(parameterName) {
    var result = null,
        tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}

function genList(options){
	var type = options.type || '';
	var title = options.title || '';
	var artis = options.artis || '';
	var url = options.url || '';
	var icon = options.icon || 'icon_kecil.png';
	var key = options.key;
	if(type == 'mp3' || type == 'fixed'){
		icon = '../'+icon;
	}
	if(type == 'mp3' || type == 'mp3-online'){
		url = '../single/index.html?k='+key+'&single='+title;
	}else if(type == 'text-online'){
		url = url;
	}else if(type == 'parsial'){
		url = 'index.html?k='+key+'&start='+options.start+'&end='+options.end;
	}
	var html = ''
        +'<li class="contentmenuitem '+type+'" data-layout="menuitem">'
            +'<a class="contentmenulink" data-layout-editable="link" href="'+url+'">'
                +'<div class="contentmenuimgcontainer">'
                    +'<img class="contentmenuitemimage" data-layout-editable="image" src="'+icon+'"/>'
                +'</div>'
                +'<div class="contentmenutextcontainer">'
                    +'<h2 class="contentmenutext1" data-layout-editable="text">'
                        +title
                    +'</h2>'
                    +'<h3 class="contentmenutext2" data-layout-editable="text">'
                    +artis
                    +'</h3>'
                +'</div>'
                +'<img align="right" class="contentmenuimgarrow" data-layout-editable="image" src="../contentmenupagearrow.png"/>'
            +'</a>'
        +'</li>';
    return html;
}

function genListIndex(options){
	var type = options.type || '';
	var title = options.title || '';
	var url = options.url || '';
	var icon = options.icon || 'icon_kecil.png';
	if(!type){
		url = 'list/index.html?k='+title;
	}
	var html = ''
        +'<li class="listmenuItem '+type+'" data-layout="menuitem">'
            +'<a class="listmenuTitle" data-layout-editable="link" href="'+url+'">'
                +'<div class="listmenuImageContainer">'
                    +'<img class="listmenuImage" data-layout-editable="image" src="'+icon+'"/>'
                +'</div>'
                +'<div>'
                    +'<h2 class="listmenu-title" data-layout-editable="text">'
                        +title
                    +'</h2>'
                +'</div>'
            +'</a>'
        +'</li>';
    return html;
}

function setToSinglePage(data){
	var key =  getParam('k');
	jQuery('#back-link').attr('href', '../list/index.html?k='+key);
	if(data.title){
		jQuery('#running-text').text(data.title);
		jQuery('#streamtitle').text(data.title);
		jQuery('#trackname').text(data.artis);
		if(data.type == 'mp3' && data.background){
			jQuery('#headerimage').attr('src', '../'+data.background);
		}else{
			jQuery('#headerimage').attr('src', data.background);
		}
		if(data.type == 'mp3'){
			jQuery('#streamlink').attr('href', 'bgplayer-play://mp3/'+data.file);
            getQuran(data);
		}else if(data.type == 'mp3-online'){
			jQuery('#streamlink').attr('href', 'bgplayer-play://'+data.url);
            setTimeout(function(){
                getQuran(data);
            }, 1000);
		}
		if(typeof Android != 'undefined'){
//		    console.log("setVpn");
//		    Android.setVpn();
			var url = 'file:///android_asset/single/index.html?k='+encodeURIComponent(key)+'&single='+encodeURIComponent(data.nextData.title)+'&autolay=1';
			Android.setNextUrl(url);
			var autolay =  getParam('autolay');
			if(typeof audio === 'undefined'){
				window.audio = {
					duration:0,
					currentTime: 0,
					play: function(){
						Android.playAudio();
					},
					pause: function(){
						Android.pauseAudio();
					},
					seekTo: function(e){
						Android.seekTo(e);
					},
					getCurrentTime: function(){
						return Android.getCurrentTime()/1000;
					}
				};
			}
			if(data.type == 'mp3'){
				audio.duration = Android.setPlayer('mp3/'+data.file)/1000;
				audio.pause();
	            jQuery('#current-time').text('0:00');
	            jQuery('#time-total').text(milliSecondsToTimer(audio.duration));
				if(autolay){
					audio.play();
				}
			}else if(data.type == 'mp3-online'){
				setTimeout(function(){
					audio.duration = Android.setPlayer(data.url)/1000;
					audio.pause();
		            jQuery('#time-total').text(milliSecondsToTimer(audio.duration));
	            	jQuery('#current-time').text('0:00');
					if(autolay){
						audio.play();
					}
		        }, 4000);
			}
			window.played = false;
			setInterval(function(){
				var isPlaying = Android.checkIsPlaying();
				if(isPlaying == 1){
					if(!played){
						playAudio({ that: document.getElementById('streamlink') });
						played = true;
					}
				}else{
					if(played && document.getElementById('stoplink').className != 'loop-button-active'){
						played = false;
						pauseAudio({ that: document.getElementById('pauselink') });
					}
				}
				var isLooping = Android.checkIsLooping();
				var that = document.getElementById('looplink');
				if(isLooping == 1){
					that.href = 'bgplayer-loop://yes';
					that.className = 'loop-button-active';
				}else{
					that.href = 'bgplayer-loop://no';
					that.className = 'loop-button';
				}
			}, 1000);
		}
	}
}

function playAudio(options){
    var e = options.e || '';
    var that = options.that;
    if(typeof Android === 'undefined'){
        e.preventDefault();
        audio.play();
    }
    document.getElementById('pauselink').style = '';
    that.className = 'loop-button-active';
    document.getElementById('pauselink').className = 'loop-button';
    document.getElementById('stoplink').className = 'loop-button';
    setBarSlider();
    setBodySlider();
}

function pauseAudio(options){
    var e = options.e || '';
    var that = options.that;
    if(typeof Android === 'undefined'){
        e.preventDefault();
        audio.pause();
    }
    document.getElementById('stoplink').className = 'loop-button';
    document.getElementById('streamlink').className = 'loop-button';
    that.className = 'loop-button-active';
    clearInterval(barSlider);
    unsetBodySlider();
}

function setBarSlider(){
    if(typeof barSlider != 'undefined'){
        clearInterval(barSlider);
    }
    window.barSlider = setInterval(function(){
        if(typeof Android !== 'undefined'){
            audio.currentTime = audio.getCurrentTime();
        }
        var width = ((audio.currentTime/audio.duration)*100)+'%';
        // console.log('audio.currentTime', audio.currentTime);
        jQuery('#audio-bar').css('width', width);
        jQuery('#range-bar').val(width.replace('%','')*1000);
        jQuery('#current-time').text(milliSecondsToTimer(audio.currentTime));
    }, 500);
}

function setBodySlider(){
    jQuery('#auto-scroll').addClass('loop-button-active');
    if(typeof bodySlider != 'undefined'){
    	clearInterval(bodySlider);
    }
    window.bodySlider = setInterval(function(){
    	var quran = jQuery('#quran');
    	var current = jQuery('#range-bar').val()/1000;
    	if(current == 100 && jQuery('#looplink').attr('class') == 'loop-button'){
        	clearInterval(barSlider);
        	unsetBodySlider();
        	return;
    	}
    	var start = quran.offset().top-100;
    	var total = quran.height();
    	var extend = total*(current/100);
    	jQuery('html, body').animate({
            scrollTop: start+extend
        }, 1000);
    }, 1000);
}

function unsetBodySlider(){
	jQuery('#auto-scroll').removeClass('loop-button-active');
    clearInterval(bodySlider);
}

function milliSecondsToTimer(milliseconds) {
    var finalTimerString = "";
    var secondsString = "";
    var hours = Math.floor(Math.round(Math.round(milliseconds)/60)/60);
    var minutes = Math.floor(Math.round(milliseconds%(60*60))/60);
    var seconds = Math.floor(milliseconds%60);
    if (hours > 0) {
        finalTimerString = hours + ":";
    }
    if (seconds < 10) {
        secondsString = "0" + seconds;
    } else {
        secondsString = "" + seconds;
    }
    finalTimerString = finalTimerString + minutes + ":" + secondsString;
    return finalTimerString;
}

function showSettings(options){
	var html = ''
		+'<div id="setting-menu">'
			+'<ul>'
	if(typeof Android != 'undefined'){
		var isScreenOn = Android.getScreenOn();
		var checkedScreenOn = '';
		if(isScreenOn == 1){
			checkedScreenOn = 'checked';
		}
		html += ''
				// +'<li for="checkbox-translation">'
				// 	+'<label>'
				// 		+'<input id="checkbox-translation" type="checkbox">'
				// 		+'Hide translation'
				// 	+'</label>'
				// +'</li>'
				+'<li for="checkbox-screen-on">'
					+'<label>'
						+'<input id="checkbox-screen-on" type="checkbox" '+checkedScreenOn+'>'
						+'Screen always ON'
					+'</label>'
				+'</li>';
				// +'<li for="checkbox-play-all">'
				// 	+'<label>'
				// 		+'<input id="checkbox-play-all" type="checkbox">'
				// 		+'Play all playlist'
				// 	+'</label>'
				// +'</li>'
				// +'<li for="checkbox-shufle">'
				// 	+'<label>'
				// 		+'<input id="checkbox-shufle" type="checkbox">'
				// 		+'Shufle playlist'
				// 	+'</label>'
				// +'</li>'
	}
	if(settings && settings[setting_local.key]){
		html += ''
				+'<li>'
					+'<a id="rate-app" href="ext-url://'+settings[setting_local.key].url+'">Give me 5 stars</a>'
				+'</li>'
			+'</ul>'
		+'</div>';
	}
	if(options.page == 'list'){
	}else if(options.page == 'single'){

	}
	jQuery('body').append(html);
}

jQuery(document).ready(function($){
	var server = 'https://maremjaya.com/android/';
	// server = 'http://192.168.43.13:8000/';
	var configUrl = server+'js/config.js?v='+(new Date()).getTime();
	var script = document.createElement('script');
	script.onload = function() {
		if(jQuery('#data_menu_index').length){
			var v = '1.0';
			if(typeof Android != "undefined"){
				var v = Android.getVersion();
			}
			if(v != settings[setting_local.key].version){
				var msg = ''
					+'<b>You are using version '+v+'.</b>'
					+'<br><b>Update to new version '+settings[setting_local.key].version+', for best experience.</b>'
					+'<br><b><a style="color: rgb(148, 2, 210);" href="ext-url://'+settings[setting_local.key].url+'">Click here!</a></b>';
				jQuery('#wrap-title').html(msg);
			}else if(settings.msg){
				jQuery('#wrap-title').html(settings.msg);
			}
			var html = '';
			config.map(function(b, i){
				if(setting_local.config_exclude.indexOf(b.title) != -1){
					return;
				}
				html += genListIndex(b);
			});
			if(jQuery('#data_menu_index .fixed').length){
				jQuery('#data_menu_index .fixed').before(html);
			}else{
				jQuery('#data_menu_index').append(html);
			}
		}
		if(jQuery('#data_menu_list').length){
			var key = getParam('k');
			jQuery('#running-text').text(key);
			var _start = getParam('start');
			var _end = getParam('end');
			var html = '';
			config.map(function(b, i){
				if(setting_local.config_exclude.indexOf(b.title) != -1){
					return;
				}
				if(b.title == key){
					var start = 0;
					var length = b.data.length;
					b.data.map(function(_b, _i){
						_b.key = key;
						var no = _i+1;
						if(!b.parsial){
							html += genList(_b);
						}else if(_start && _end){
							if(no>=_start && no<=_end){
								html += genList(_b);
							}
						}else{
							if(_i!=0){
								if(no%b.max==0 || no==length){
									_b.start = start;
									_b.end = no;
									start = _b.end+1;
									_b.title = 'Quran Surah '+_b.start+' - '+_b.end;
									_b.type = 'parsial';
									_b.artis = 'Shaikh Mishary Rashid Alafasy';
									html += genList(_b);
								}
							}else{
								start = no;
							}
						}
					});
				}
				if(b.type == "fixed"){
	                html += genList(b);
	            }
			});
			if(jQuery('#data_menu_list .fixed').length){
				jQuery('#data_menu_list .fixed').before(html);
			}else{
				jQuery('#data_menu_list').append(html);
			}
		}
		if(jQuery('#single-page').length){
			var key =  getParam('k');
			var single =  getParam('single');
			var data = {};
			config.map(function(b, i){
				if(setting_local.config_exclude.indexOf(b.title) != -1){
					return;
				}
				if(b.title == key){
					var l = b.data.length;
					b.data.map(function(_b, _i){
						if(_b.title == single){
							data = _b;
							var nextData = {};
							if(l-1 == _i){
								nextData = b.data[0];
							}else{
								nextData = b.data[_i+1];
							}
							data.nextData = nextData;
						}
					});
				}
			});
			if(data.title){
				setToSinglePage(data);
			}
		}
	};
	script.addEventListener("unload", function() {
		var title = 'Data not loaded cause you are offline or the server is down! Restart in few minute.';
		if(jQuery('#data_menu_index').length){
			var html = ''
		        +'<li class="listmenuItem" data-layout="menuitem">'
		            +'<a class="listmenuTitle" data-layout-editable="link" href="#">'
		                +'<div class="listmenuImageContainer">'
		                +'</div>'
		                +'<div>'
		                    +'<h2 class="listmenu-title" data-layout-editable="text">'
		                        +title
		                    +'</h2>'
		                +'</div>'
		            +'</a>'
		        +'</li>';
			if(jQuery('#data_menu_index .fixed').length){
				jQuery('#data_menu_index .fixed').before(html);
			}else{
				jQuery('#data_menu_index').append(html);
			}
		}
		if(jQuery('#data_menu_list').length){
			var html = ''
		        +'<li class="contentmenuitem" data-layout="menuitem">'
		            +'<a class="contentmenulink" data-layout-editable="link" href="#">'
		                +'<div class="contentmenuimgcontainer">'
		                +'</div>'
		                +'<div class="contentmenutextcontainer">'
		                    +'<h2 class="contentmenutext1" data-layout-editable="text">'
		                        +title
		                    +'</h2>'
		                +'</div>'
		                +'<img align="right" class="contentmenuimgarrow" data-layout-editable="image" src="../contentmenupagearrow.png"/>'
		            +'</a>'
		        +'</li>';
			if(jQuery('#data_menu_list .fixed').length){
				jQuery('#data_menu_list .fixed').before(html);
			}else{
				jQuery('#data_menu_list').append(html);
			}
		}
		if(jQuery('#single-page').length){
			jQuery('#streamtitle').text(title);
		}
	});
	script.src = configUrl;
	document.getElementsByTagName('head')[0].appendChild(script);
	if(jQuery('#data_menu_index').length){
		var html = '';
		config_local.map(function(b, i){
			html += genListIndex(b);
		});
		jQuery('#data_menu_index').html(html);
	}
	if(jQuery('#data_menu_list').length){
		var key = getParam('k');
		jQuery('#running-text').text(key);
		var _start = getParam('start');
		var _end = getParam('end');
		var html = '';
		config_local.map(function(b, i){
			if(b.title == key){
				var start = 0;
				var length = b.data.length;
				b.data.map(function(_b, _i){
					_b.key = key;
					var no = _i+1;
					if(!b.parsial){
						html += genList(_b);
					}else if(_start && _end){
						if(no>=_start && no<=_end){
							html += genList(_b);
						}
					}else{
						if(_i!=0){
							if(no%b.max==0 || no==length){
								_b.start = start;
								_b.end = no;
								start = _b.end+1;
								_b.title = 'Quran Surah '+_b.start+' - '+_b.end;
								_b.type = 'parsial';
								_b.artis = 'Shaikh Mishary Rashid Alafasy';
								html += genList(_b);
							}
						}else{
							start = no;
						}
					}
				});
			}
			if(b.type == "fixed"){
                html += genList(b);
            }
		});
		jQuery('#data_menu_list').html(html);
	}
	if(jQuery('#single-page').length){
		var key =  getParam('k');
		var single =  getParam('single');
		var data = {};
		config_local.map(function(b, i){
			if(b.title == key){
				var l = b.data.length;
				b.data.map(function(_b, _i){
					if(_b.title == single){
						data = _b;
						var nextData = {};
						if(l-1 == _i){
							nextData = b.data[0];
						}else{
							nextData = b.data[_i+1];
						}
						data.nextData = nextData;
					}
				});
			}
		});
		setToSinglePage(data);
	}
	jQuery(document).click(function(e){
	    var clicked = e.target.id;
	    if(
	    	clicked != 'settings'
	    	&& jQuery(e.target).closest('#settings').length == 0
	    ){
			var menuID = "setting-menu";
		    var myTarget = jQuery('#'+menuID);
	    	if(myTarget.length) {
		        if(
		        	menuID != clicked
		        	&& jQuery(e.target).closest('#'+menuID).length == 0
		        ) {
		            jQuery("#setting-menu").remove();
		        }
		    }
	    }
	});
	jQuery('body').on('click', '#checkbox-translation', function(){

	});
	jQuery('body').on('click', '#rate-app', function(){
		jQuery("#setting-menu").remove();
	});
	jQuery('body').on('click', '#checkbox-screen-on', function(){
		var isScreenOn = jQuery(this).is(':checked');
		if(isScreenOn){
			Android.setScreenOn(1);
		}else{
			Android.setScreenOn(0);
		}
		jQuery("#setting-menu").remove();
	});
});