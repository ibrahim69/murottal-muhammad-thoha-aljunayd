package com.maremjaya.murrotalusthananattaki;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.VpnService;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RemoteViews;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by agus on 11/01/18.
 */

public class MainActivity  extends Activity {

    private InterstitialAd mInterstitialAd;
    private AdView mdView;
    private Handler mHandler;
    static Boolean isLoop = false;
    private final static String APP_PNAME = "com.maremjaya.murrotalusthananattaki";// Package Name
    //    String KEYWORD = "mesothelioma law firm,donate car to charity california,donate car for tax credit,donate cars in ma,donate your car sacramento,how to donate a car in california,sell annuity payment,donate your car for kids,asbestos lawyers,structured annuity settlement,annuity settlements,car insurance quotes colorado,nunavut culture,dayton freight lines,harddrive data recovery services,donate a car in maryland,motor replacements,cheap domain registration hosting,donating a car in maryland,donate cars illinois,criminal defense attorneys florida,best criminal lawyer in arizona,car insurance quotes utah,life insurance co lincoln,holland michigan college,online motor insurance quotes,online colledges,paperport promotional code,onlineclasses,world trade center footage,massage school dallas texas,psychic for free,donate old cars to charity,low credit line credit cards,dallas mesothelioma attorneys,car insurance quotes mn,donate your car for money,cheap auto insurance in va,met auto,forensics online course,home phone internet bundle,donating used cars to charity,phd in counseling education,neuson,car insurance quotes pa,royalty free images stock,car insurance in south dakota,email bulk service,webex costs,cheap car insurance for ladies,insurance,treatment,loans,attorney,mortgage,hosting,rehab,classes,transfer,recovery,software,claim,trading,lawyer,donate, credit,conference call, degree $40.61,gas/electricity,cord blood";
    String KEYWORD = "donate car";
    WebView web;
    String baseUrl = "file:///android_asset/index.html";
    static MediaPlayer mp = new MediaPlayer();
    private Runnable displayAd;
    static int length = 0;
    static String url = "";
    OkHttpClient client = new OkHttpClient();

    Context mContext=MainActivity.this;
    SharedPreferences appPreferences;
    boolean isAppInstalled = false;
    private final BroadcastReceiver mybroadcast = new ActionReceiver();
    static RemoteViews notificationView;
    static Notification.Builder drivingNotifBldr;
    static NotificationManager mgr;
    static Boolean mpCheck = false;
    AdRequest adRequest;
    static String next_url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        appPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Boolean isScreenKeepOn = appPreferences.getBoolean("isScreenKeepOn",true);
        if(isScreenKeepOn){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }else{
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
        addShortcutIcon();

        web = (WebView) findViewById(R.id.webview);
        web.getSettings().setJavaScriptEnabled(true);
        web.addJavascriptInterface(new WebAppInterface(this), "Android");

        Intent i = getIntent();
        if(i.hasExtra("url")){
            Bundle bundle = i.getExtras();
            url  = bundle.getString("url");
        }else {
            url = baseUrl;
        }
        web.loadUrl(url);
        web.setWebViewClient(new MainActivity.myWebClient());

        Boolean isAppInstalled = appPreferences.getBoolean("isAppInstalled",false);
        if(isAppInstalled==true) {
            mInterstitialAd = newInterstitialAd();
            mHandler = new Handler();
            displayAd = new Runnable() {
                public void run() {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }
                }
            };
            loadAd();
            displayInterstitial();
        }else{
            SharedPreferences.Editor editor = appPreferences.edit();
            editor.putBoolean("isAppInstalled", true);
            editor.commit();
        }

        mdView = (AdView) findViewById (R.id.adView);
        adRequest = new AdRequest.Builder()
                .addKeyword(KEYWORD)
                .build();
        mdView.loadAd(adRequest);
    }

    public class WebAppInterface {
        Context mContext;

        WebAppInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public String getQuran(String url) {
            Log.d("getQuran", url+"");
            String getResponse = doGetRequest(url);
            return getResponse;
        }

        @JavascriptInterface
        public String getVersion() throws PackageManager.NameNotFoundException {
            return getApplicationContext()
                    .getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName;
        }

        @JavascriptInterface
        public int setPlayer(String _url) throws IOException {
            Log.d("setPlayer", _url+"");
            Uri sound;
            mp.reset();
            if (_url.indexOf("://") != -1) {
                mp.setDataSource(getBaseContext(), Uri.parse(_url));
            } else {
                AssetFileDescriptor afd = getAssets().openFd(_url);
                mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                afd.close();
            }
            mp.prepare();
            mp.setLooping(isLoop);
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    if(!isLoop){
                        Intent i = new Intent(getBaseContext(), MainActivity.class);
                        Bundle data = new Bundle();
                        if(!next_url.equals("")) {
                            data.putString("url", next_url);
                        }else{
                            data.putString("url", url);
                        }
                        i.putExtras(data);
                        finish();
                        startActivity(i);
                    }
                }
            });
            mp.start();
            mpCheck = true;
            return mp.getDuration();
        }

        @JavascriptInterface
        public int getCurrentTime(){
            length = mp.getCurrentPosition();
            return length;
        }

        @JavascriptInterface
        public int checkIsPlaying(){
            if(mp.isPlaying()){
                return 1;
            }else{
                return 0;
            }
        }

        @JavascriptInterface
        public int checkIsLooping(){
            if(mp.isLooping()){
                return 1;
            }else{
                return 0;
            }
        }

        @JavascriptInterface
        public void pauseAudio(){
            mp.pause();
            length = mp.getCurrentPosition();
        }

        @JavascriptInterface
        public void playAudio(){
            mp.start();
        }

        @JavascriptInterface
        public void seekTo(int l){
            mp.seekTo(l*1000);
        }

        @JavascriptInterface
        public void setScreenOn(int data){
            SharedPreferences.Editor editor = appPreferences.edit();
            if(data == 1) {
                editor.putBoolean("isScreenKeepOn", true);
            }else{
                editor.putBoolean("isScreenKeepOn", false);
            }
            editor.commit();
        }

        @JavascriptInterface
        public int getScreenOn(){
            Boolean isScreenKeepOn = appPreferences.getBoolean("isScreenKeepOn",true);
            if(isScreenKeepOn == true){
                return 1;
            }else{
                return 0;
            }
        }
        @JavascriptInterface
        public void setNextUrl(String _url){
            next_url = _url;
        }
        @JavascriptInterface
        public void setVpn(){
            Intent intent = VpnService.prepare(getApplicationContext());
            if (intent != null) {
                startActivityForResult(intent, 0);
            } else {
                onActivityResult(0, RESULT_OK, null);
            }
        }
    }

    public void showPlayNotification(){
        notificationView = new RemoteViews(getPackageName(),
                R.layout.notification);

        PendingIntent pPlay = PendingIntent.getBroadcast(
                getBaseContext(),
                0,
                new Intent(getBaseContext(),ActionReceiver.class)
                        .setAction("start"),
                0);
        notificationView.setOnClickPendingIntent(R.id.play, pPlay);

        PendingIntent pPause = PendingIntent.getBroadcast(
                getBaseContext(),
                0,
                new Intent(getBaseContext(),ActionReceiver.class)
                        .setAction("pause"),
                0);
        notificationView.setOnClickPendingIntent(R.id.pause, pPause);

        PendingIntent pHome = PendingIntent.getBroadcast(
                getBaseContext(),
                0,
                new Intent(getBaseContext(),ActionReceiver.class)
                        .setAction("home"),
                0);
        notificationView.setOnClickPendingIntent(R.id.notifiation_image, pHome);

        PendingIntent pStop = PendingIntent.getBroadcast(
                getBaseContext(),
                0,
                new Intent(getBaseContext(),ActionReceiver.class)
                        .setAction("stop"),
                0);
        notificationView.setOnClickPendingIntent(R.id.stop, pStop);

        PendingIntent pLoop = PendingIntent.getBroadcast(
                getBaseContext(),
                0,
                new Intent(getBaseContext(),ActionReceiver.class)
                        .setAction("loop"),
                0);
        notificationView.setOnClickPendingIntent(R.id.loop, pLoop);

        PendingIntent pOther = PendingIntent.getBroadcast(
                getBaseContext(),
                0,
                new Intent(getBaseContext(),ActionReceiver.class)
                        .setAction("other"),
                0);
        notificationView.setOnClickPendingIntent(R.id.other, pOther);

        if(mp.isPlaying()) {
            notificationView.setInt(R.id.play,
                    "setBackgroundColor",
                    R.color.btnActive);
        }else {
            if(length > 0) {
                notificationView.setInt(R.id.pause,
                        "setBackgroundColor",
                        R.color.btnActive);
            }else {
                notificationView.setInt(R.id.stop,
                        "setBackgroundColor",
                        R.color.btnActive);
            }
        }
        if(mp.isLooping()) {
            notificationView.setInt(R.id.loop,
                    "setBackgroundColor",
                    R.color.btnActive);
        }
        PendingIntent pDeleteView = PendingIntent.getBroadcast(
                getBaseContext(),
                0,
                new Intent(getBaseContext(),ActionReceiver.class)
                        .setAction("delete"),
                0);

        drivingNotifBldr = new Notification.Builder(getBaseContext())
                .setSmallIcon(R.drawable.play)
                .setContent(notificationView)
                .setDeleteIntent(pDeleteView);
        // untuk mematenkan notification agar tidak bisa diclose
//                .setOngoing(true);
        mgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (mgr != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                drivingNotifBldr.setPriority(Notification.PRIORITY_MAX);
                mgr.notify(1, drivingNotifBldr.build());
            }
        }

    }

    public static class ActionReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Boolean closeNotif = true;
            String action=intent.getAction();
            Log.d("onReceive", action);
            NotificationManager mgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if(action.equals("pause")){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && mpCheck) {
                    mp.pause();
                    length = mp.getCurrentPosition();
                    notificationView.setInt(R.id.pause,
                            "setBackgroundResource",
                            R.color.btnActive);
                    notificationView.setInt(R.id.play,
                            "setBackgroundResource",
                            R.color.btnDefault);
                    notificationView.setInt(R.id.stop,
                            "setBackgroundResource",
                            R.color.btnDefault);
                    mgr.notify(1, drivingNotifBldr.build());
                    closeNotif = false;
                }else{
                    mgr.cancel(1);
                    PackageManager pm = context.getPackageManager();
                    Intent launchIntent = pm.getLaunchIntentForPackage(APP_PNAME);
                    context.startActivity(launchIntent);
                }
            }else if(action.equals("stop")){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && mpCheck) {
                    mp.pause();
                    length = 0;
                    notificationView.setInt(R.id.pause,
                            "setBackgroundResource",
                            R.color.btnDefault);
                    notificationView.setInt(R.id.play,
                            "setBackgroundResource",
                            R.color.btnDefault);
                    notificationView.setInt(R.id.stop,
                            "setBackgroundResource",
                            R.color.btnActive);
                    mgr.notify(1, drivingNotifBldr.build());
                    closeNotif = false;
                }else{
                    mgr.cancel(1);
                    PackageManager pm = context.getPackageManager();
                    Intent launchIntent = pm.getLaunchIntentForPackage(APP_PNAME);
                    context.startActivity(launchIntent);
                }
            }else if(action.equals("loop")){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && mpCheck) {
                    if(!mp.isLooping()) {
                        mp.setLooping(true);
                        notificationView.setInt(R.id.loop,
                                "setBackgroundResource",
                                R.color.btnActive);
                    }else{
                        mp.setLooping(false);
                        notificationView.setInt(R.id.loop,
                                "setBackgroundResource",
                                R.color.btnDefault);
                    }
                    mgr.notify(1, drivingNotifBldr.build());
                    closeNotif = false;
                }else{
                    mgr.cancel(1);
                    PackageManager pm = context.getPackageManager();
                    Intent launchIntent = pm.getLaunchIntentForPackage(APP_PNAME);
                    context.startActivity(launchIntent);
                }
            }else if(action.equals("start")){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && mpCheck) {
                    notificationView.setInt(R.id.play,
                            "setBackgroundResource",
                            R.color.btnActive);
                    notificationView.setInt(R.id.pause,
                            "setBackgroundResource",
                            R.color.btnDefault);
                    notificationView.setInt(R.id.stop,
                            "setBackgroundResource",
                            R.color.btnDefault);
                    mgr.notify(1, drivingNotifBldr.build());
                    mp.seekTo(length);
                    mp.start();
                    closeNotif = false;
                }else{
                    mgr.cancel(1);
                    PackageManager pm = context.getPackageManager();
                    Intent launchIntent = pm.getLaunchIntentForPackage(APP_PNAME);
                    context.startActivity(launchIntent);
                }
            }else if(action.equals("home")){
                PackageManager pm = context.getPackageManager();
                Intent launchIntent = pm.getLaunchIntentForPackage(APP_PNAME);
                context.startActivity(launchIntent);
            }else if(action.equals("other")){
                Uri _url = Uri.parse("https://play.google.com/store/apps/details?id="+APP_PNAME);
                context.startActivity(new Intent(Intent.ACTION_VIEW, _url).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
            }else if(action.equals("delete")){
                if(mpCheck){
                    mp.pause();
                    length = 0;
                }
                closeNotif = false;
            }

            if(closeNotif) {
                //This is used to close the notification tray
                Intent it = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                context.sendBroadcast(it);
            }
        }

    }

    public String doGetRequest(String url){
        try {
            Request request = new Request.Builder()
                    .url(url)
                    .build();
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
            return "error";
        }
    }

    public class myWebClient extends WebViewClient
    {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String _url) {
            // TODO Auto-generated method stub
            Log.d("url", _url);
            if(_url.startsWith("bgplayer-play://")) {
                _url = _url.substring(16);
                try {
                    mp.seekTo(length);
                    mp.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else if(_url.startsWith("bgplayer-loop://yes")){
                mp.setLooping(true);
                isLoop = true;
                Log.d("setLooping", "true");
            }else if(_url.startsWith("bgplayer-loop://no")){
                mp.setLooping(false);
                isLoop = false;
                Log.d("setLooping", "false");
            }else if(_url.startsWith("bgplayer-pause://")){
                mp.pause();
                length = mp.getCurrentPosition();
            }else if(_url.startsWith("bgplayer-stop://")){
                mp.pause();
                length = 0;
            }else if(_url.startsWith("ext-url://")){
                _url = _url.substring(10);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(_url)));
            }else if(_url.endsWith("&in-url=1")){
                return true;
            }else {
                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    mp.pause();
                    length = 0;
                }
                Intent i = new Intent(getBaseContext(), MainActivity.class);
                Bundle data = new Bundle();
                data.putString("url", _url);
                i.putExtras(data);
                finish();
                startActivity(i);
            }
            return true;

        }
    }

    private InterstitialAd newInterstitialAd() {
        InterstitialAd interstitialAd= new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded(){ }

            @Override
            public void onAdFailedToLoad(int errorCode){ }

            @Override
            public void onAdClosed(){ }
        });
        return interstitialAd;

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            web.loadUrl(baseUrl);
            if(!mp.isPlaying() || Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                mp.pause();
                length = 0;
            }
            if(!url.equals(baseUrl)) {
                Intent i = new Intent(getBaseContext(), MainActivity.class);
                Bundle data = new Bundle();
                data.putString("url", baseUrl);
                i.putExtras(data);
                finish();
                startActivity(i);
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    void loadAd() {
        AdRequest adRequest = new AdRequest.Builder()
                //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .setRequestAgent("android_studio:ad_template")
                .addKeyword(KEYWORD)
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    //Call displayInterstitial() once you are ready to display the ad.
    public void displayInterstitial() {
        if(url.indexOf("/single/") != -1) {
            mHandler.postDelayed(displayAd, 60000);
        }else{
            mHandler.postDelayed(displayAd, 10000);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mdView != null){
            mdView.loadAd(adRequest);
        }
        if(mpCheck && mp.getDuration() > 0 && url.indexOf("/single/") != -1 && mgr != null) {
            mgr.cancel(1);
        }
    }

    @Override
    public void onPause() {
        //Toast.makeText(getBaseContext(),"onPause",Toast.LENGTH_SHORT).show();
        if(mdView !=null) {
            mdView.destroy();
        }
        if(mHandler != null) {
            mHandler.removeCallbacks(displayAd);
        }
        super.onPause();
        if(mp != null && mp.getDuration() > 0) {
            showPlayNotification();
        }
    }

    @Override
    public void onStop(){
        //Toast.makeText(getBaseContext(),"onStop",Toast.LENGTH_SHORT).show();
        if(mdView !=null) {
            mdView.destroy();
        }
        if(mHandler != null) {
            mHandler.removeCallbacks(displayAd);
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if(mdView !=null) {
            mdView.destroy();
        }
        if(mHandler != null) {
            mHandler.removeCallbacks(displayAd);
        }
        super.onDestroy();
    }

    // onClick of addShortcutIcon
    private void addShortcutIcon() {
        //shorcutIntent object
        Intent shortcutIntent = new Intent(getApplicationContext(),
                MainActivity.class);
        shortcutIntent.setAction(Intent.ACTION_MAIN);

        //shortcutIntent is added with addIntent
        Intent addIntent = new Intent();
        addIntent.putExtra("duplicate", false);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, R.string.app_name);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                Intent.ShortcutIconResource.fromContext(getApplicationContext(),
                        R.mipmap.ic_launcher));
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");

        // finally broadcast the new Intent
        getApplicationContext().sendBroadcast(addIntent);
        Log.d("addshortcut", "yes");
//        setResult(RESULT_OK, addIntent);
    }

    //    https://stackoverflow.com/questions/13133498/how-to-download-mp3-file-in-android-from-a-url-and-save-it-in-sd-card-here-is
    public String getFileMp3(String _url){
        try{

            File cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"MaremJayaMp3");
            if(!cacheDir.exists())
                cacheDir.mkdirs();

            String songname[] = _url.split("/");
            File f=new File(cacheDir,songname[songname.length-1]);
            if(!f.exists()) {
                URL url = new URL(_url);

                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(f);

                byte data[] = new byte[1024];
                long total = 0;
                int count=0;
                while ((count = input.read(data)) != -1) {
                    total++;
                    Log.e("while","A"+total);

                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();
            }
            return f.getAbsolutePath();
        }catch(Exception e){
            e.printStackTrace();
            return "";
        }
    }
}

