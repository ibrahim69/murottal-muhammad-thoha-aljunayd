var config_local = [{
    title: 'Murotal Ustadz Hanan Attaki Offline',
    icon: 'icon_kecil.png',
    background: 'background_player.png',
    data: [{
        type: 'mp3',
        title: 'SURAT AN-NABA',
        detail: {
            taawaud: true,
            basmalah: true,
            alfatihah_no_basmalah: true,
            local: surah_local[78]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '78. An-Naba.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AN-NAAZIAT',
        detail: {
            basmalah: true,
            local: surah_local[79]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '079_An-Naziat.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT ABASA',
        detail: {
            basmalah: true,
            local: surah_local[80]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '080_Abasa.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AT-TAKWIR',
        detail: {
            basmalah: true,
            local: surah_local[81]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '081_At-Takwir.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-INFITHAR',
        detail: {
            basmalah: true,
            local: surah_local[82]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '082_Al-Infitar.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-MUTAFFIFIN',
        detail: {
            basmalah: true,
            local: surah_local[83]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '083_Al-Mutaffifin.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-INSYIQOQ',
        detail: {
            basmalah: true,
            local: surah_local[84]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '084_Al-Insyiqaq.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-BURUJ',
        detail: {
            basmalah: true,
            local: surah_local[85]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '085_Al-Buruj.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AT-THORIQ',
        detail: {
            basmalah: true,
            local: surah_local[86]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '086_At-Tariq.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL ALA',
        detail: {
            basmalah: true,
            local: surah_local[87]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '087_Al-Ala.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-GHOSIYAH',
        detail: {
            basmalah: true,
            local: surah_local[88]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '088_Al-Ghasiyah.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-FAJR',
        detail: {
            basmalah: true,
            local: surah_local[89]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '089_Al-Fajr.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-BALAD',
        detail: {
            basmalah: true,
            local: surah_local[90]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '090_Al-Balad.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AS-SYAMS',
        detail: {
            basmalah: true,
            local: surah_local[91]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '091_As-ySyams.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-LAIL',
        detail: {
            basmalah: true,
            local: surah_local[92]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '092_Al-Lail.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AD-DHUHA',
        detail: {
            basmalah: true,
            local: surah_local[93]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '093_Adh-Dhuha.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AS-SYARH',
        detail: {
            basmalah: true,
            local: surah_local[94]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '094_Al-Insyirah.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AT-TIIN',
        detail: {
            basmalah: true,
            local: surah_local[95]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '095_At-Tin.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-ALAQ',
        detail: {
            basmalah: true,
            local: surah_local[96]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '096_Al-Alaq.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-QODAR',
        detail: {
            basmalah: true,
            local: surah_local[97]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '097_Al-Qadr.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT BAYYINAH',
        detail: {
            basmalah: true,
            local: surah_local[98]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '098_Al-Bayyinah.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-ZALZALAH',
        detail: {
            basmalah: true,
            local: surah_local[99]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '099_Az-Zalzalah.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-ADIYAT',
        detail: {
            basmalah: true,
            local: surah_local[100]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '100_Al-Adiyat.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'    
    }, {
        type: 'mp3',
        title: 'SURAT AL-QORIAH',
        detail: {
            basmalah: true,
            local: surah_local[101]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '101_Al-Qariah.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AT-TAKAATSUR',
        detail: {
            basmalah: true,
            local: surah_local[102]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '102_At-Takatsur.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-ASHR',
        detail: {
            basmalah: true,
            local: surah_local[103]
        },
        artis: 'UMUHAMMAD THOHA ALJUNAYD',
        file: '103_Al-Ashr.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-HUMAZAH',
        detail: {
            basmalah: true,
            local: surah_local[104]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '104_Al-Humazah.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-FIIL',
        detail: {
            basmalah: true,
            local: surah_local[105]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '105_Al-Fil.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-QURAISH',
        detail: {
            basmalah: true,
            local: surah_local[106]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '106_Al-Quraish.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-MAAUN',
        detail: {
            basmalah: true,
            local: surah_local[107]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '107_Al-Maun.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-KAUTSAR',
        detail: {
            basmalah: true,
            local: surah_local[108]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '108_Al-Kautsar.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-KAAFIRUN',
        detail: {
            basmalah: true,
            local: surah_local[109]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '109_Al-Kafiruni.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AN-NASHR',
        detail: {
            basmalah: true,
            local: surah_local[110]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '110_An-Nasr.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-MASAD',
        detail: {
            basmalah: true,
            local: surah_local[111]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '111_Al-Lahab.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-IKHLAS',
        detail: {
            basmalah: true,
            local: surah_local[112]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '112_Al-Ikhlas.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AL-FALAQ',
        detail: {
            basmalah: true,
            local: surah_local[113]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '113_Al-Falaq.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }, {
        type: 'mp3',
        title: 'SURAT AN-NAAS',
        detail: {
            local: surah_local[114]
        },
        artis: 'MUHAMMAD THOHA ALJUNAYD',
        file: '114_An-Nas.mp3',
        icon: 'icon_kecil.png',
        background: 'background_player.png'
    }]
}, {
    type: 'fixed',
    url: 'ext-url://https://play.google.com/store/apps/developer?id=Marem+Jaya+Apps',
    title: 'Aplikasi Islami Lainnya',
    artis: '',
    "icon": "icon_kecil3.png",
}];
var setting_local = {
    key: 'abu_usamah',
    config_exclude: ['Murotal Ustadz Hanan Attaki Online']
};